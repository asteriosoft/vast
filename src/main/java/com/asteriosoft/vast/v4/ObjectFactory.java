/*
 *
 *  * Copyright 2023 Asteriosoft Inc.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.asteriosoft.vast.v4;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public final class ObjectFactory {

    public VAST createVAST() {
        return new VAST();
    }

    public MediaFilesType createMediaFilesType() {
        return new MediaFilesType();
    }

    public IconsType createIconsType() {
        return new IconsType();
    }

    public IconsType.Icon createIconsTypeIcon() {
        return new IconsType.Icon();
    }

    public TrackingEventsNonLinearType createTrackingEventsNonLinearType() {
        return new TrackingEventsNonLinearType();
    }

    public AdVerificationsType createAdVerificationsType() {
        return new AdVerificationsType();
    }

    public AdVerificationsType.Verification createAdVerificationsTypeVerification() {
        return new AdVerificationsType.Verification();
    }

    public TrackingEventsLinearType createTrackingEventsLinearType() {
        return new TrackingEventsLinearType();
    }

    public AdVerificationsWrapperType createAdVerificationsWrapperType() {
        return new AdVerificationsWrapperType();
    }

    public CreativeExtensionsType createCreativeExtensionsType() {
        return new CreativeExtensionsType();
    }

    public TrackingEventsCompanionType createTrackingEventsCompanionType() {
        return new TrackingEventsCompanionType();
    }

    public ExtensionsType createExtensionsType() {
        return new ExtensionsType();
    }

    public VAST.Ad createVASTAd() {
        return new VAST.Ad();
    }

    public VAST.Ad.Wrapper createVASTAdWrapper() {
        return new VAST.Ad.Wrapper();
    }

    public VAST.Ad.Wrapper.Creatives createVASTAdWrapperCreatives() {
        return new VAST.Ad.Wrapper.Creatives();
    }

    public VAST.Ad.Wrapper.Creatives.Creative createVASTAdWrapperCreativesCreative() {
        return new VAST.Ad.Wrapper.Creatives.Creative();
    }

    public VAST.Ad.InLine createVASTAdInLine() {
        return new VAST.Ad.InLine();
    }

    public VAST.Ad.InLine.Creatives createVASTAdInLineCreatives() {
        return new VAST.Ad.InLine.Creatives();
    }

    public VAST.Ad.InLine.Creatives.Creative createVASTAdInLineCreativesCreative() {
        return new VAST.Ad.InLine.Creatives.Creative();
    }

    public StaticResourceType createStaticResourceType() {
        return new StaticResourceType();
    }

    public VideoClicksType createVideoClicksType() {
        return new VideoClicksType();
    }

    public UniversalAdIdType createUniversalAdIdType() {
        return new UniversalAdIdType();
    }

    public CompanionWrapperType createCompanionWrapperType() {
        return new CompanionWrapperType();
    }

    public AdSystemType createAdSystemType() {
        return new AdSystemType();
    }

    public NonLinearType createNonLinearType() {
        return new NonLinearType();
    }

    public InteractiveCreativeFileType createInteractiveCreativeFileType() {
        return new InteractiveCreativeFileType();
    }

    public HTMLResourceType createHTMLResourceType() {
        return new HTMLResourceType();
    }

    public PricingType createPricingType() {
        return new PricingType();
    }

    public NonLinearWrapperType createNonLinearWrapperType() {
        return new NonLinearWrapperType();
    }

    public SurveyType createSurveyType() {
        return new SurveyType();
    }

    public AnyURIType createAnyURIType() {
        return new AnyURIType();
    }

    public CategoryType createCategoryType() {
        return new CategoryType();
    }

    public AdParametersType createAdParametersType() {
        return new AdParametersType();
    }

    public CompanionType createCompanionType() {
        return new CompanionType();
    }

    public VideoClicksWrapperType createVideoClicksWrapperType() {
        return new VideoClicksWrapperType();
    }

    public ViewableImpressionType createViewableImpressionType() {
        return new ViewableImpressionType();
    }

    public MediaFilesType.MediaFile createMediaFilesTypeMediaFile() {
        return new MediaFilesType.MediaFile();
    }

    public IconsType.Icon.IconClicks createIconsTypeIconIconClicks() {
        return new IconsType.Icon.IconClicks();
    }

    public TrackingEventsNonLinearType.Tracking createTrackingEventsNonLinearTypeTracking() {
        return new TrackingEventsNonLinearType.Tracking();
    }

    public AdVerificationsType.Verification.JavaScriptResource createAdVerificationsTypeVerificationJavaScriptResource() {
        return new AdVerificationsType.Verification.JavaScriptResource();
    }

    public AdVerificationsType.Verification.FlashResource createAdVerificationsTypeVerificationFlashResource() {
        return new AdVerificationsType.Verification.FlashResource();
    }

    public TrackingEventsLinearType.Tracking createTrackingEventsLinearTypeTracking() {
        return new TrackingEventsLinearType.Tracking();
    }

    public AdVerificationsWrapperType.Verification createAdVerificationsWrapperTypeVerification() {
        return new AdVerificationsWrapperType.Verification();
    }

    public CreativeExtensionsType.CreativeExtension createCreativeExtensionsTypeCreativeExtension() {
        return new CreativeExtensionsType.CreativeExtension();
    }

    public TrackingEventsCompanionType.Tracking createTrackingEventsCompanionTypeTracking() {
        return new TrackingEventsCompanionType.Tracking();
    }

    public ExtensionsType.Extension createExtensionsTypeExtension() {
        return new ExtensionsType.Extension();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.Linear createVASTAdWrapperCreativesCreativeLinear() {
        return new VAST.Ad.Wrapper.Creatives.Creative.Linear();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.CompanionAds createVASTAdWrapperCreativesCreativeCompanionAds() {
        return new VAST.Ad.Wrapper.Creatives.Creative.CompanionAds();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.NonLinearAds createVASTAdWrapperCreativesCreativeNonLinearAds() {
        return new VAST.Ad.Wrapper.Creatives.Creative.NonLinearAds();
    }

    public VAST.Ad.InLine.Creatives.Creative.Linear createVASTAdInLineCreativesCreativeLinear() {
        return new VAST.Ad.InLine.Creatives.Creative.Linear();
    }

    public VAST.Ad.InLine.Creatives.Creative.CompanionAds createVASTAdInLineCreativesCreativeCompanionAds() {
        return new VAST.Ad.InLine.Creatives.Creative.CompanionAds();
    }

    public VAST.Ad.InLine.Creatives.Creative.NonLinearAds createVASTAdInLineCreativesCreativeNonLinearAds() {
        return new VAST.Ad.InLine.Creatives.Creative.NonLinearAds();
    }

}
