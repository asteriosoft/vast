/*
 *
 *  * Copyright 2023 Asteriosoft Inc.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.asteriosoft.vast.v3;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public final class ObjectFactory {

    public VAST createVAST() {
        return new VAST();
    }

    public NonLinearType createNonLinearType() {
        return new NonLinearType();
    }

    public CompanionType createCompanionType() {
        return new CompanionType();
    }

    public IconType createIconType() {
        return new IconType();
    }

    public CompanionWrapperType createCompanionWrapperType() {
        return new CompanionWrapperType();
    }

    public TrackingEventsType createTrackingEventsType() {
        return new TrackingEventsType();
    }

    public CreativeExtensionsType createCreativeExtensionsType() {
        return new CreativeExtensionsType();
    }

    public VideoClicksType createVideoClicksType() {
        return new VideoClicksType();
    }

    public ExtensionsType createExtensionsType() {
        return new ExtensionsType();
    }

    public VAST.Ad createVASTAd() {
        return new VAST.Ad();
    }

    public VAST.Ad.Wrapper createVASTAdWrapper() {
        return new VAST.Ad.Wrapper();
    }

    public VAST.Ad.Wrapper.Creatives createVASTAdWrapperCreatives() {
        return new VAST.Ad.Wrapper.Creatives();
    }

    public VAST.Ad.Wrapper.Creatives.Creative createVASTAdWrapperCreativesCreative() {
        return new VAST.Ad.Wrapper.Creatives.Creative();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.Linear createVASTAdWrapperCreativesCreativeLinear() {
        return new VAST.Ad.Wrapper.Creatives.Creative.Linear();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks createVASTAdWrapperCreativesCreativeLinearVideoClicks() {
        return new VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks();
    }

    public VAST.Ad.InLine createVASTAdInLine() {
        return new VAST.Ad.InLine();
    }

    public VAST.Ad.InLine.Creatives createVASTAdInLineCreatives() {
        return new VAST.Ad.InLine.Creatives();
    }

    public VAST.Ad.InLine.Creatives.Creative createVASTAdInLineCreativesCreative() {
        return new VAST.Ad.InLine.Creatives.Creative();
    }

    public VAST.Ad.InLine.Creatives.Creative.Linear createVASTAdInLineCreativesCreativeLinear() {
        return new VAST.Ad.InLine.Creatives.Creative.Linear();
    }

    public VAST.Ad.InLine.Creatives.Creative.Linear.MediaFiles createVASTAdInLineCreativesCreativeLinearMediaFiles() {
        return new VAST.Ad.InLine.Creatives.Creative.Linear.MediaFiles();
    }

    public AdParametersType createAdParametersType() {
        return new AdParametersType();
    }

    public ImpressionType createImpressionType() {
        return new ImpressionType();
    }

    public AdSystemType createAdSystemType() {
        return new AdSystemType();
    }

    public HTMLResourceType createHTMLResourceType() {
        return new HTMLResourceType();
    }

    public NonLinearWrapperType createNonLinearWrapperType() {
        return new NonLinearWrapperType();
    }

    public StaticResource createNonLinearTypeStaticResource() {
        return new StaticResource();
    }

    public StaticResource createCompanionTypeStaticResource() {
        return new StaticResource();
    }

    public StaticResource createIconTypeStaticResource() {
        return new StaticResource();
    }

    public IconType.IconClicks createIconTypeIconClicks() {
        return new IconType.IconClicks();
    }

    public StaticResource createCompanionWrapperTypeStaticResource() {
        return new StaticResource();
    }

    public TrackingEventsType.Tracking createTrackingEventsTypeTracking() {
        return new TrackingEventsType.Tracking();
    }

    public CreativeExtensionsType.CreativeExtension createCreativeExtensionsTypeCreativeExtension() {
        return new CreativeExtensionsType.CreativeExtension();
    }

    public VideoClicksType.ClickThrough createVideoClicksTypeClickThrough() {
        return new VideoClicksType.ClickThrough();
    }

    public VideoClicksType.ClickTracking createVideoClicksTypeClickTracking() {
        return new VideoClicksType.ClickTracking();
    }

    public VideoClicksType.CustomClick createVideoClicksTypeCustomClick() {
        return new VideoClicksType.CustomClick();
    }

    public ExtensionsType.Extension createExtensionsTypeExtension() {
        return new ExtensionsType.Extension();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.CompanionAds createVASTAdWrapperCreativesCreativeCompanionAds() {
        return new VAST.Ad.Wrapper.Creatives.Creative.CompanionAds();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.NonLinearAds createVASTAdWrapperCreativesCreativeNonLinearAds() {
        return new VAST.Ad.Wrapper.Creatives.Creative.NonLinearAds();
    }

    public Icons createVASTAdWrapperCreativesCreativeLinearIcons() {
        return new Icons();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks.ClickTracking createVASTAdWrapperCreativesCreativeLinearVideoClicksClickTracking() {
        return new VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks.ClickTracking();
    }

    public VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks.CustomClick createVASTAdWrapperCreativesCreativeLinearVideoClicksCustomClick() {
        return new VAST.Ad.Wrapper.Creatives.Creative.Linear.VideoClicks.CustomClick();
    }

    public VAST.Ad.InLine.Pricing createVASTAdInLinePricing() {
        return new VAST.Ad.InLine.Pricing();
    }

    public VAST.Ad.InLine.Creatives.Creative.CompanionAds createVASTAdInLineCreativesCreativeCompanionAds() {
        return new VAST.Ad.InLine.Creatives.Creative.CompanionAds();
    }

    public VAST.Ad.InLine.Creatives.Creative.NonLinearAds createVASTAdInLineCreativesCreativeNonLinearAds() {
        return new VAST.Ad.InLine.Creatives.Creative.NonLinearAds();
    }

    public Icons createVASTAdInLineCreativesCreativeLinearIcons() {
        return new Icons();
    }

    public VAST.Ad.InLine.Creatives.Creative.Linear.MediaFiles.MediaFile createVASTAdInLineCreativesCreativeLinearMediaFilesMediaFile() {
        return new VAST.Ad.InLine.Creatives.Creative.Linear.MediaFiles.MediaFile();
    }

}
