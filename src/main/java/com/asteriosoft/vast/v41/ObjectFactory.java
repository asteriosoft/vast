/*
 *
 *  * Copyright 2023 Asteriosoft Inc.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.asteriosoft.vast.v41;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.iab.vast package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.iab.vast
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VAST }
     * 
     */
    public VAST createVAST() {
        return new VAST();
    }

    /**
     * Create an instance of {@link VideoClicksBaseType }
     * 
     */
    public VideoClicksBaseType createVideoClicksBaseType() {
        return new VideoClicksBaseType();
    }

    /**
     * Create an instance of {@link AdDefinitionBaseType }
     * 
     */
    public AdDefinitionBaseType createAdDefinitionBaseType() {
        return new AdDefinitionBaseType();
    }

    /**
     * Create an instance of {@link InlineType }
     * 
     */
    public InlineType createInlineType() {
        return new InlineType();
    }

    /**
     * Create an instance of {@link AdDefinitionBaseType.Extensions }
     * 
     */
    public AdDefinitionBaseType.Extensions createAdDefinitionBaseTypeExtensions() {
        return new AdDefinitionBaseType.Extensions();
    }

    /**
     * Create an instance of {@link CreativeResourceType }
     * 
     */
    public CreativeResourceType createCreativeResourceType() {
        return new CreativeResourceType();
    }

    /**
     * Create an instance of {@link CompanionAdType }
     * 
     */
    public CompanionAdType createCompanionAdType() {
        return new CompanionAdType();
    }

    /**
     * Create an instance of {@link IconType }
     * 
     */
    public IconType createIconType() {
        return new IconType();
    }

    /**
     * Create an instance of {@link TrackingEventsVerificationType }
     * 
     */
    public TrackingEventsVerificationType createTrackingEventsVerificationType() {
        return new TrackingEventsVerificationType();
    }

    /**
     * Create an instance of {@link LinearBaseType }
     * 
     */
    public LinearBaseType createLinearBaseType() {
        return new LinearBaseType();
    }

    /**
     * Create an instance of {@link NonLinearAdInlineType }
     * 
     */
    public NonLinearAdInlineType createNonLinearAdInlineType() {
        return new NonLinearAdInlineType();
    }

    /**
     * Create an instance of {@link CreativeInlineType }
     * 
     */
    public CreativeInlineType createCreativeInlineType() {
        return new CreativeInlineType();
    }

    /**
     * Create an instance of {@link VideoClicksInlineType }
     * 
     */
    public VideoClicksInlineType createVideoClicksInlineType() {
        return new VideoClicksInlineType();
    }

    /**
     * Create an instance of {@link TrackingEventsType }
     * 
     */
    public TrackingEventsType createTrackingEventsType() {
        return new TrackingEventsType();
    }

    /**
     * Create an instance of {@link CreativeExtensionsType }
     * 
     */
    public CreativeExtensionsType createCreativeExtensionsType() {
        return new CreativeExtensionsType();
    }

    /**
     * Create an instance of {@link WrapperType }
     * 
     */
    public WrapperType createWrapperType() {
        return new WrapperType();
    }

    /**
     * Create an instance of {@link VerificationType }
     * 
     */
    public VerificationType createVerificationType() {
        return new VerificationType();
    }

    /**
     * Create an instance of {@link NonLinearAdBaseType }
     * 
     */
    public NonLinearAdBaseType createNonLinearAdBaseType() {
        return new NonLinearAdBaseType();
    }

    /**
     * Create an instance of {@link LinearInlineType }
     * 
     */
    public LinearInlineType createLinearInlineType() {
        return new LinearInlineType();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles }
     * 
     */
    public LinearInlineType.MediaFiles createLinearInlineTypeMediaFiles() {
        return new LinearInlineType.MediaFiles();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles.ClosedCaptionFiles }
     * 
     */
    public LinearInlineType.MediaFiles.ClosedCaptionFiles createLinearInlineTypeMediaFilesClosedCaptionFiles() {
        return new LinearInlineType.MediaFiles.ClosedCaptionFiles();
    }

    /**
     * Create an instance of {@link CreativeWrapperType }
     * 
     */
    public CreativeWrapperType createCreativeWrapperType() {
        return new CreativeWrapperType();
    }

    /**
     * Create an instance of {@link VAST.Ad }
     * 
     */
    public VAST.Ad createVASTAd() {
        return new VAST.Ad();
    }

    /**
     * Create an instance of {@link LinearWrapperType }
     * 
     */
    public LinearWrapperType createLinearWrapperType() {
        return new LinearWrapperType();
    }

    /**
     * Create an instance of {@link CreativeBaseType }
     * 
     */
    public CreativeBaseType createCreativeBaseType() {
        return new CreativeBaseType();
    }

    /**
     * Create an instance of {@link CompanionAdsCollectionType }
     * 
     */
    public CompanionAdsCollectionType createCompanionAdsCollectionType() {
        return new CompanionAdsCollectionType();
    }

    /**
     * Create an instance of {@link HTMLResourceType }
     * 
     */
    public HTMLResourceType createHTMLResourceType() {
        return new HTMLResourceType();
    }

    /**
     * Create an instance of {@link AdVerificationsType }
     * 
     */
    public AdVerificationsType createAdVerificationsType() {
        return new AdVerificationsType();
    }

    /**
     * Create an instance of {@link ImpressionType }
     * 
     */
    public ImpressionType createImpressionType() {
        return new ImpressionType();
    }

    /**
     * Create an instance of {@link AdParametersType }
     * 
     */
    public AdParametersType createAdParametersType() {
        return new AdParametersType();
    }

    /**
     * Create an instance of {@link IconClickTrackingType }
     * 
     */
    public IconClickTrackingType createIconClickTrackingType() {
        return new IconClickTrackingType();
    }

    /**
     * Create an instance of {@link ViewableImpressionType }
     * 
     */
    public ViewableImpressionType createViewableImpressionType() {
        return new ViewableImpressionType();
    }

    /**
     * Create an instance of {@link VideoClicksBaseType.ClickTracking }
     * 
     */
    public VideoClicksBaseType.ClickTracking createVideoClicksBaseTypeClickTracking() {
        return new VideoClicksBaseType.ClickTracking();
    }

    /**
     * Create an instance of {@link VideoClicksBaseType.CustomClick }
     * 
     */
    public VideoClicksBaseType.CustomClick createVideoClicksBaseTypeCustomClick() {
        return new VideoClicksBaseType.CustomClick();
    }

    /**
     * Create an instance of {@link AdDefinitionBaseType.AdSystem }
     * 
     */
    public AdDefinitionBaseType.AdSystem createAdDefinitionBaseTypeAdSystem() {
        return new AdDefinitionBaseType.AdSystem();
    }

    /**
     * Create an instance of {@link AdDefinitionBaseType.Pricing }
     * 
     */
    public AdDefinitionBaseType.Pricing createAdDefinitionBaseTypePricing() {
        return new AdDefinitionBaseType.Pricing();
    }

    /**
     * Create an instance of {@link InlineType.Category }
     * 
     */
    public InlineType.Category createInlineTypeCategory() {
        return new InlineType.Category();
    }

    /**
     * Create an instance of {@link InlineType.Creatives }
     * 
     */
    public InlineType.Creatives createInlineTypeCreatives() {
        return new InlineType.Creatives();
    }

    /**
     * Create an instance of {@link InlineType.Survey }
     * 
     */
    public InlineType.Survey createInlineTypeSurvey() {
        return new InlineType.Survey();
    }

    /**
     * Create an instance of {@link AdDefinitionBaseType.Extensions.Extension }
     * 
     */
    public AdDefinitionBaseType.Extensions.Extension createAdDefinitionBaseTypeExtensionsExtension() {
        return new AdDefinitionBaseType.Extensions.Extension();
    }

    /**
     * Create an instance of {@link CreativeResourceType.StaticResource }
     * 
     */
    public CreativeResourceType.StaticResource createCreativeResourceTypeStaticResource() {
        return new CreativeResourceType.StaticResource();
    }

    /**
     * Create an instance of {@link CompanionAdType.CompanionClickTracking }
     * 
     */
    public CompanionAdType.CompanionClickTracking createCompanionAdTypeCompanionClickTracking() {
        return new CompanionAdType.CompanionClickTracking();
    }

    /**
     * Create an instance of {@link IconType.IconClicks }
     * 
     */
    public IconType.IconClicks createIconTypeIconClicks() {
        return new IconType.IconClicks();
    }

    /**
     * Create an instance of {@link TrackingEventsVerificationType.Tracking }
     * 
     */
    public TrackingEventsVerificationType.Tracking createTrackingEventsVerificationTypeTracking() {
        return new TrackingEventsVerificationType.Tracking();
    }

    /**
     * Create an instance of {@link LinearBaseType.Icons }
     * 
     */
    public LinearBaseType.Icons createLinearBaseTypeIcons() {
        return new LinearBaseType.Icons();
    }

    /**
     * Create an instance of {@link NonLinearAdInlineType.NonLinearClickTracking }
     * 
     */
    public NonLinearAdInlineType.NonLinearClickTracking createNonLinearAdInlineTypeNonLinearClickTracking() {
        return new NonLinearAdInlineType.NonLinearClickTracking();
    }

    /**
     * Create an instance of {@link CreativeInlineType.NonLinearAds }
     * 
     */
    public CreativeInlineType.NonLinearAds createCreativeInlineTypeNonLinearAds() {
        return new CreativeInlineType.NonLinearAds();
    }

    /**
     * Create an instance of {@link CreativeInlineType.UniversalAdId }
     * 
     */
    public CreativeInlineType.UniversalAdId createCreativeInlineTypeUniversalAdId() {
        return new CreativeInlineType.UniversalAdId();
    }

    /**
     * Create an instance of {@link VideoClicksInlineType.ClickThrough }
     * 
     */
    public VideoClicksInlineType.ClickThrough createVideoClicksInlineTypeClickThrough() {
        return new VideoClicksInlineType.ClickThrough();
    }

    /**
     * Create an instance of {@link TrackingEventsType.Tracking }
     * 
     */
    public TrackingEventsType.Tracking createTrackingEventsTypeTracking() {
        return new TrackingEventsType.Tracking();
    }

    /**
     * Create an instance of {@link CreativeExtensionsType.CreativeExtension }
     * 
     */
    public CreativeExtensionsType.CreativeExtension createCreativeExtensionsTypeCreativeExtension() {
        return new CreativeExtensionsType.CreativeExtension();
    }

    /**
     * Create an instance of {@link WrapperType.BlockedAdCategories }
     * 
     */
    public WrapperType.BlockedAdCategories createWrapperTypeBlockedAdCategories() {
        return new WrapperType.BlockedAdCategories();
    }

    /**
     * Create an instance of {@link WrapperType.Creatives }
     * 
     */
    public WrapperType.Creatives createWrapperTypeCreatives() {
        return new WrapperType.Creatives();
    }

    /**
     * Create an instance of {@link VerificationType.ExecutableResource }
     * 
     */
    public VerificationType.ExecutableResource createVerificationTypeExecutableResource() {
        return new VerificationType.ExecutableResource();
    }

    /**
     * Create an instance of {@link VerificationType.JavaScriptResource }
     * 
     */
    public VerificationType.JavaScriptResource createVerificationTypeJavaScriptResource() {
        return new VerificationType.JavaScriptResource();
    }

    /**
     * Create an instance of {@link NonLinearAdBaseType.NonLinearClickTracking }
     * 
     */
    public NonLinearAdBaseType.NonLinearClickTracking createNonLinearAdBaseTypeNonLinearClickTracking() {
        return new NonLinearAdBaseType.NonLinearClickTracking();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles.MediaFile }
     * 
     */
    public LinearInlineType.MediaFiles.MediaFile createLinearInlineTypeMediaFilesMediaFile() {
        return new LinearInlineType.MediaFiles.MediaFile();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles.Mezzanine }
     * 
     */
    public LinearInlineType.MediaFiles.Mezzanine createLinearInlineTypeMediaFilesMezzanine() {
        return new LinearInlineType.MediaFiles.Mezzanine();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles.InteractiveCreativeFile }
     * 
     */
    public LinearInlineType.MediaFiles.InteractiveCreativeFile createLinearInlineTypeMediaFilesInteractiveCreativeFile() {
        return new LinearInlineType.MediaFiles.InteractiveCreativeFile();
    }

    /**
     * Create an instance of {@link LinearInlineType.MediaFiles.ClosedCaptionFiles.ClosedCaptionFile }
     * 
     */
    public LinearInlineType.MediaFiles.ClosedCaptionFiles.ClosedCaptionFile createLinearInlineTypeMediaFilesClosedCaptionFilesClosedCaptionFile() {
        return new LinearInlineType.MediaFiles.ClosedCaptionFiles.ClosedCaptionFile();
    }

    /**
     * Create an instance of {@link CreativeWrapperType.NonLinearAds }
     * 
     */
    public CreativeWrapperType.NonLinearAds createCreativeWrapperTypeNonLinearAds() {
        return new CreativeWrapperType.NonLinearAds();
    }

}
